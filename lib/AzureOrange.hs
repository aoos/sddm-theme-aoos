-- ColorScheme = DoomOne (until color16)

module Colors.AzureOrange where

import XMonad

colorScheme = "doom-one"

colorBack = "#282c34"
colorFore = "#bbc2cf"

color01 = "#1c1f24" -- greyish blue
color02 = "#ff6c6b" -- salmon red
color03 = "#98be65" -- yellowish green
color04 = "#da8548" -- orange?
color05 = "#51afef" -- light blue
color06 = "#c678dd" -- violet
color07 = "#5699af" -- turqoise wtf?
color08 = "#202328" -- dark grey
color09 = "#5b6268" -- light grey
color10 = "#da8548" -- orange again?
color11 = "#4db5bd" -- greenish light blue
color12 = "#ecbe7b" -- light orange
color13 = "#3071db" -- blue
color14 = "#a9a1e1" -- light violet?
color15 = "#46d9ff" -- blueish light green
color16 = "#dfdfdf" -- redish white
color17 = "#94a187" -- greenish grey
color18 = "#c5afa0" -- redish grey
azure   = "#007fff"
orange  = "#ff9700"

colorTrayer :: String
colorTrayer = "--tint 0x282c34"

